; types
;
; name
; text
; uint8
; uint16
; uint32
; time (uint32 with variant presentation format)
; ipv4
; ipv6
; flags16
; base64 (whitespace allowed)
; hex (no whitespace allowed)
; unknown
; rrtype
; bitlist

; qualifiers
;
; name
;  host
;  mail
;  compressed
;  additional
; uint32
;  interval (BIND extension)
; bitlist
;  port
;  rrtype
; text
;  multiple

define uint8 algo [http://www.iana.org/assignments/dns-sec-alg-numbers/dns-sec-alg-numbers.xml]
 1   RSAMD5            -
 2   DH                -
 3   DSA               -
 5   RSASHA1           -
 6   DSA-NSEC3-SHA1    -
 7  RSASHA1-NSEC3-SHA1 -
 8   RSASHA256         -
 10  RSASHA512         -
 12  ECC-GOST          -
 13  ECDSAP256SHA256   -
 14  ECDSAP384SHA384   -
 252 INDIRECT          -
 252 PRIVATEDNS        -
 253 PRIVATEOID        -

TYPE1 A IPv4 address [RFC1035]
 ipv4 ADDRESS -

TYPE2 NS Name server [RFC1035]
 name NSDNAME Host name
  host additional compressed

TYPE3 MD Mail destination (OBSOLETE - use MX) [RFC1035]
 name MADNAME Mail agent host name
  host additional compressed

TYPE4 MF Mail forwarder (OBSOLETE - use MX) [RFC1035]
 name MADNAME Mail agent host name
  host additional compressed

TYPE5 CNAME Alias [RFC1035]
 name CNAME Canonical name
  compressed

TYPE6 SOA Start of zone of authority [RFC1035]
 name   MNAME   Master name server
  host compressed
 name   RNAME   Hostmaster email address
  mail compressed
 uint32 SERIAL  Zone serial number
 uint32 REFRESH Refresh interval
  interval
 uint32 RETRY   Retry interval
  interval
 uint32 EXPIRE  Slave copy lifetime
  interval
 uint32 MINIMUM Negative response TTL
  interval

TYPE7 MB Mailbox (OBSOLETE - use MX) [RFC1035]
 name MADNAME Mail agent host name
  host additional compressed

TYPE8 MG Mail group (OBSOLETE - use MX) [RFC1035]
 name MGNAME Group member email address
  mail compressed

TYPE9 MR Mail rename (OBSOLETE - use MX) [RFC1035]
 name NEWNAME Replacement email address
  mail compressed

TYPE10 NULL Meaningless data [RFC1035]

TYPE11 WKS Well known service (OBSOLETE) [RFC1035]
 ipv4   ADDRESS  Host address
 uint16 PROTOCOL Protocol
  6  TCP -
  17 UDP -
 bitlist - Bit map of open ports

TYPE12 PTR Pointer from reverse DNS [RFC1035]
 name PTRDNAME Forward DNS name
  host compressed
; is host flag correct?

TYPE13 HINFO Host information [RFC1035]
 text CPU -
 text OS  -

TYPE14 MINFO Mail information (OBSOLETE - use MX) [RFC1035]
 name RMAILBX Responsible person email address
  mail compressed
 name EMAILBX Error email address
  mail compressed

TYPE15 MX Mail exchanger [RFC1035]
 uint16 PREFERENCE Preference (lower values tried first)
 name   EXCHANGE   Mail exchange host name
; host additional compressed

TYPE16 TXT Text [RFC1035]
 text TXT-DATA -
  multiple

TYPE17 RP Responsible person [RFC1183]
 name MBOX-DNAME Email address
  mail
 name TXT-DNAME  Pointer to TXT records

TYPE18 AFSDB AFS database location [RFC1183]
 uint16 SUBTYPE -
  1 - AFSv3 volume location server
  2 - cell root directory node
 name HOSTNAME -
  host additional

TYPE19 X25 X.25 PSDN address (OBSOLETE) [RFC1183]
 text PSDN-ADDRESS -

TYPE20 ISDN ISDN address (OBSOLETE) [RFC1183]
 text ISDN-ADDRESS Telephone number
 text SA           Subaddress
  optional

TYPE21 RT Route through [RFC1183]
 uint16 PREFERENCE        Preference (lower values tried first)
 name   INTERMEDIATE-HOST Intermediate host name
  host additional

TYPE22 NSAP ISO NSAP address (OBSOLETE) [RFC1706]
; hex NSAP-ADDRESS -
;  with 0x prefix and optional dots

TYPE23 NSAP-PTR ISO NSAP reverse pointer (OBSOLETE - use PTR) [RFC1348]
 name OWNER -

TYPE24 SIG Cryptographic signature (OBSOLETE) [RFC2535]
 rrtype TYPE-COVERED Type covered
 algo   ALGORITHM    Signature algorithm
 uint8  LABELS       Non-wildcard label count
 uint32 ORIG-TTL     Original TTL
  interval
 time   EXPIRATION   Expiration time
 time   INCEPTION    Inception time
 uint16 KEY-TAG      Key tag
 name   SIGNER       Key name
 base64 SIGNATURE    Signature data

TYPE25 KEY Cryptographic key [RFC2535]
 flags16 FLAGS Flags
  49152 A-C    Key restrictions
   49152 NOKEY  No key present
   32769 NOAUTH Not for authentication
   16384 NOCONF Not for encryption
  4096  EXTEND Extended flags
  768   NAMTYP Owner type
   768   OTHER  -
   512   HOST   -
   256   ZONE   -
   0     USER   -
  15    SIG    Signatory [RFC2137]
  8     ZONE    Zone control
  4     STRONG  Strong update
  2     UNIQUE  Unique name update
  1     GENERAL General update
 uint8 PROTOCOL Protocol
  1 TLS    -
  2 EMAIL  -
  3 DNSSEC -
  4 IPSEC  -
  255 ALL  -
 algo   ALGORITHM  Algorithm
 base64 PUBLIC-KEY Public key data

TYPE26 PX X.400 mail gateway [RFC2163]
 uint16 PREFERENCE Preference (lower values tried first)
 name   MAP822  -
 name   MAPX400 -

TYPE27 GPOS Geographical Position [RFC1712]
 text LONGITUDE -
 text LATITUDE  -
 text ALTITUDE  -

TYPE28 AAAA IPv6 address [RFC3596]
 ipv6 ADDRESS -

TYPE29 LOC Location [RFC1876]
; uint8  VERSION   Must be 0
; float8 SIZE      Diameter
; float8 HORIZ-PRE Horizontal precision
; float8 VERT-PRE  Vertical precision
; angle  LATITUDE  Latitude
;  N,S
; angle  LONGITURE Longitude
;  E,W
; altitude ALTITUDE Altitude
;
; angle is uint32 thousandths of a second of arc plus 2^31
; presented as degrees, minutes, seconds
; altitude is uint32 centimetres plus 10,000,000
; presented as metres
;
; presentation order is
; latitude
; longitude
; altitude
; size      (default 1m)
; horiz-pre (default 10000m)
; vert-pre  (default 10m)

TYPE30 NXT Next domain (OBSOLETE) [RFC2535]
 name NEXT Next domain name
 bitlist
  rrtype

TYPE31 EID Endpoint Identifier

TYPE32 NIMLOC Nimrod Locator

TYPE33 SRV      Service location [RFC2782]
 uint16 PRIORITY Preference (lower values tried first)
 uint16 WEIGHT   -
 uint16 PORT     -
 name   TARGET   -
  host additional

TYPE34 ATMA ATM address

TYPE35 NAPTR Naming authority pointer [RFC3403]
 uint16 ORDER      Order (lower values processed first)
 uint16 PREFERENCE Preference (lower values tried first)
 text  FLAGS       -
 text  SERVICES    -
 text  REGEXP      Regular expression
 name  REPLACEMENT -

TYPE36 KX Key exchanger [RFC2230]
 uint16 PREFERENCE Preference (lower values tried first)
 name   EXCHANGER  Host name
  host additional

TYPE37 CERT Cryptographic certificate [RFC4398]
 uint16 TYPE      -
    1    PKIX      X.509 as per PKIX
    2    SPKI      SPKI certificate
    3    PGP       OpenPGP packet
    4    IPKIX     The URL of an X.509 data object
    5    ISPKI     The URL of an SPKI certificate
    6    IPGP      The fingerprint and URL of an OpenPGP packet
    7    ACPKIX    Attribute Certificate
    8    IACPKIX   The URL of an Attribute Certificate
    253  URI       Type described by URI
    254  OID       Type described by OID
 uint16 KEY-TAG   Key tag
 algo   ALGORITHM Algorithm
 base64 -         Certificate or CRL data

TYPE38 A6 Partial IPv6 address (OBSOLETE - use AAAA) [RFC2874]
 a6   ADDRESS     Prefix length and address suffix
 name PREFIX-NAME Prefix name
  optional

TYPE39 DNAME Subdomain redirect [RFC2672]
 name DNAME Target domain

TYPE40 SINK Kitchen sink
 uint16 CODING    -
 uint16 SUBCODING -
 base64 DATA      -

TYPE41 OPT Extension pseudo-record [RFC2671]

TYPE42 APL Address prefix list [RFC3123]
; Complicated syntax
; repeat
;  uint16 ADDRESSFAMILY
;   1 - IPv4
;   2 - IPv6
;  flags16 - -
;   65280  PREFIX    -
;   128    N         -
;   12 7   AFDLENGTH -
;  ipv4/ipv6 AFDPART -

TYPE43 DS Delegation signer [RFC4034]
 uint16 KEY-TAG     Key tag
 algo   ALGORITHM   Algorithm
 uint8  DIGEST-TYPE Digest type
  1 SHA-1   -
  2 SHA-256 -
  3 GOST    -
  4 SHA-384 -
 base64 DIGEST      Key digest

TYPE44 SSHFP Secure shell key fingerprint [RFC4255]
 uint8 ALGORITHM Algorithm
  1 RSA   -
  2 DSS   -
 uint8 FP-TYPE   Fingerprint type
  1 SHA-1 -
 hex FINGERPRINT Fingerprint

TYPE45 IPSECKEY IPSEC public key [RFC4025]
; uint8 PRECEDENCE   Preference (lower values tried first)
; uint8 GATEWAY-TYPE -
;  1 - IPv4 address
;  2 - IPv6 address
;  3 - Domain name
; uint8 ALGORITHM -
;  1 DSA - [RFC2536]
;  2 RSA - [RFC3110]
; ipv4/ipv6/name GATEWAY -
; base64 PUBLIC-KEY -

TYPE46 RRSIG DNSSEC resource record signature [RFC4034]
 rrtype TYPE-COVERED Type covered
 algo   ALGORITHM    Signature algorithm
 uint8  LABELS       Non-wildcard label count
 uint32 ORIG-TTL     Original TTL
  interval
 time   EXPIRATION   Expiration time
 time   INCEPTION    Inception time
 uint16 KEY-TAG      Key tag
 name   SIGNER       Key name
 base64 SIGNATURE    Signature data

TYPE47 NSEC DNSSEC next resource record [RFC4034]
 name NEXT  Next domain name
 nsec TYPES Resource record types present

TYPE48 DNSKEY DNSSEC public key [RFC4034]
 flags16 FLAGS Flags
  256 - Zone key
  128 - Revoked
  1   - Secure entry point (KSK)
 uint8 PROTOCOL Protocol
  3 - DNSSEC
 algo   ALGORITHM  Algorithm
 base64 PUBLIC-KEY Public key data

TYPE49 DHCID DHCP client identifier [RFC4701]
 base64 - -

TYPE50 NSEC3 DNSSEC hashed resource record existence [RFC5155]
; uint8  - Hash algorithm
;  1 - SHA-1
; flags8 - Flags
; uint16 - Iterations
; uint8  - Salt length
; hex    - Salt
; uint8  - Hash length
; base32 - Next hashed owner name
; nsec   - Resource record types present

TYPE51 NSEC3PARAM NSEC3 parameters [RFC5155]
; uint8  - Hash algorithm
;  1 - SHA-1
; flags8 - Flags
; uint16 - Iterations
; uint8  - Salt length
; hex    - Salt

TYPE55 HIP Host Identity Protocol [RFC5205]
; uint8  HIT-LEN HIT length (missing in presentation format)
; uint8  PK-ALGO Public key algorithm
; uint16 PK-LEN  Public key length (missing in presentation format)
; hex    HIT     -
; base64 PK      Public key (no whitespace allowed)
; name   -       Rendezvous servers
;  multiple

TYPE99 SPF Mail sender policy framework [RFC4408]
 text TXT-DATA -
  multiple
